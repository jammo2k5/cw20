How to install :

1.Put "cw2020" folder in mods\update\x64\dlcpacks
2.Add this line -> dlcpacks:\cw2020\ to the dlclist.xml (mods\update\update.rpf\common\data)

Changelog : 1.1
Added liveries from Bytx.

1.1.1
Fixed Dashboard.
Fixed vehicle class.
Unable to fix the engine off (My fix for fivem dosent work in SP).

1.1.2
Actually fixed vehicle class this time.

Livery packs are planned by other creators.
If you want more track orientated handling reduce the bracking force to about 0.2 in the handling.meta

Credits :
Jam - Model, Porting, Embrace Livery
Bytx - All other liveries
Lambofreak / 3P1C / Piggy / Tim - Sound
Jonny362000 - Handling

This model is release under a GPLv3 License, Use of the model for anything is permitted however it is to be released in an unlocked format with any modifications you do also in an unlocked format. 